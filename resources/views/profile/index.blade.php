@extends('layout.master')
@section('Judul')
    <h3>Profile</h3>
@endsection
@section('content')
<div class="container">
    <form action="/profile/{{ $profile->id }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" disabled value="{{ $profile->user->name }}">
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" disabled value="{{ $profile->user->email }}">
        </div>

        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name="umur" value="{{ $profile->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" name="alamat" class="form-control">{{ $profile->alamat }}</textarea>
        </div>

        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

    </form>
</div>
@endsection
