@extends('layout.master')
@section('Judul')
    <h3>Detail Produk</h3>
@endsection
@push('style')
    <link href="{{asset('famms/css/keranjang.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush

@section('content')
<div class="container">
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 mb-4 mb-lg-0">
                    <div class="product-slider" id="galleryTop">
                        <div class="swiper-container theme-slider position-lg-absolute all-0"
                            data-swiper='{"autoHeight":true,"spaceBetween":5,"loop":true,"loopedSlides":5,"thumb":{"spaceBetween":5,"slidesPerView":5,"loop":true,"freeMode":true,"grabCursor":true,"loopedSlides":5,"centeredSlides":true,"slideToClickedSlide":true,"watchSlidesVisibility":true,"watchSlidesProgress":true,"parent":"#galleryTop"},"slideToClickedSlide":true}'>
                            <div class="swiper-wrapper h-100">
                                <div class="swiper-slide h-100"><img class="rounded-1 fit-cover h-100 w-100" src="{{asset('gambar/'. $produk->foto)}}" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h5>{{$produk->nama}}</h5>
                    <a class="fs--1 mb-2 d-block" href="/kategori/{{$produk->kategori_id}}">
                        <span class="badge badge-info">{{$produk->kategori->nama}}</span></a>
                    <h4 class="d-flex align-items-center"><span class="text-warning me-2">Rp. {{$produk->harga}}</span>
                    </h4>
                    <hr>
                     {{Str::limit($produk->deskripsi, 40)}}
                    <hr>
                    @auth
                    <form action="/like" method="post">
                        @csrf
                        <input type="hidden" name="is_like" value="1">
                        <input type="hidden" name="produk_id" value="{{$produk->id}}"> 
                        <button type="submit" class="btn btn-primary">Like <i class="fa-solid fa-thumbs-up"></i> (<span class="badged badge-light">{{$total_like}}</span>)</button> 
                        {{-- ({{$like->is_like_count}}) --}}
                    </form>
                    <div class="row mt-3">
                            <form action="/order" method="post">
                                @csrf
                                    <div class="quantity mb-4">
                                        <input type="number" name="quantity" min="1" step="1" value="0" style="width:75px; height: 35px;" >
                                    </div></br>
                                    <input type="hidden" name="produk_id" value="{{$produk->id}}">

                                    <input type="submit" class="btn btn-sm btn-primary" name="kirim" value="Add To Cart">
                            </form>
                            @endauth
                            @guest
                            <form>
                                @csrf
                                <input type="hidden" name="is_like" value="1">
                                <input type="hidden" name="produk_id" value="{{$produk->id}}"> 
                                <a href="/login" class="btn btn-primary">Like <i class="fa-solid fa-thumbs-up"></i> (<span class="badged badge-light">{{$total_like}}</span>)</a> 
                            </form>
                            <div class="col-auto pe-0 mt-3">
                                <div class="quantity">
                                    <input type="number" min="1" step="1" value="0" style="width:75px; height: 35px;">
                                </div>
                            </div>
                            <div class="col-auto px-2 px-md-3">
                                <a class="btn btn-sm btn-primary" href="/login">
                                    <span class="d-none d-sm-inline-block">
                                        Add To Cart
                                    </span>
                                </a>
                            </div>
                            @endguest
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="overflow-hidden mt-4">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active ps-0" id="description-tab" data-toggle="tab"
                                    href="#tab-description" role="tab" aria-controls="tab-description"
                                    aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link px-2 px-md-3" id="reviews-tab" data-toggle="tab"
                                    href="#tab-reviews" role="tab" aria-controls="tab-reviews"
                                    aria-selected="false">Reviews</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="tab-description" role="tabpanel"
                                aria-labelledby="description-tab">
                                <div class="mt-3">
                                    {{$produk->deskripsi}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-reviews" role="tabpanel"
                                aria-labelledby="reviews-tab">
                                <div class="row mt-3">
                                    <div class="col-lg-6 mb-4 mb-lg-0">
                                        @foreach ($produk->komentar as $item)
                                        <p class="fs--1 mb-2 text-600">By {{$item->user->name}} • {{$item->created_at}}</p>
                                        <p class="mb-0">{{$item->isi}}</p>
                                        <hr class="my-4" />
                                        @endforeach
                                    </div>
                                    <div class="col-lg-6 ps-lg-5">
                                        @auth
                                        <form action="/komentar" method="post">
                                            @csrf
                                            <h5 class="mb-3">Write your Komentar</h5>
                                            <input type="hidden" name="produk_id" value="{{$produk->id}}">
                                            <div class="mb-3">
                                                <label class="form-label" for="formGrouptextareaInput">Komentar:</label>
                                                    <textarea class="form-control" id="formGrouptextareaInput" name="isi" rows="3"></textarea>

                                            </div>
                                            @error('isi')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </form>
                                        @endauth
                                        @guest
                                        <form >
                                            <h5 class="mb-3">Write your Komentar</h5>
                                            <div class="mb-3">
                                                <label class="form-label" for="formGrouptextareaInput">Komentar:</label>
                                                    <textarea class="form-control" id="formGrouptextareaInput" name="isi" rows="3" disabled>Silahkan Login untuk komentar</textarea>

                                            </div>
                                        </form>
                                        @endguest
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="{{asset('famms/js/keranjang.js')}}"></script>
@endpush

