@extends('layout.master')
@section('judul')
Produk
@endsection
@section('content')


<section class="product_section layout_padding">
    <div class="container">
        <div class="heading_container heading_center">
            <h2>
               Our <span>products</span>
            </h2>
         </div>
       <div class="row">

        @forelse($produk as $item)
          <div class="col-sm-6 col-md-4 col-lg-3">
             <div class="box">
                <div class="option_container">
                   <div class="options">
                    <a href="/produk/{{$item->id}}" class="option1">
                        Detail
                        </a>
                    <a href="/produk/{{$item->id}}" class="option2">
                        Buy Now
                        </a>
                    @if (Auth::check()&&Auth::user()->is_admin!=null)
                      <a href="/produk/create" class="btn btn-primary btn-sm option3"> Add Produk</a>
                        <a href="/produk/{{$item->id}}/edit" class="btn btn-warning btn-sm option3">Edit</a>
                        <form class="mt-2" action="/produk/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm option3">
                    @endif

                   </div>
                </div>
                <div class="img-box">
                   <img src="{{asset('gambar/'. $item->foto)}}" alt="">
                </div>
                <div class="detail-box">
                   <h5>
                    {{$item->nama}}
                   </h5>
                   <h6>
                    Rp. {{$item->harga}}
                   </h6>
                </div>
             </div>
          </div>
        @empty
            <h4>Produk Belum ada </h4>
            @if (Auth::check()&&Auth::user()->is_admin!=null)
            <a href="/produk/create" class="btn btn-primary">Tambah Produk</a>
            @endif
        @endforelse
       </div>
    </div>
 </section>
 @include('sweetalert::alert')
@endsection
