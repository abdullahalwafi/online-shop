@extends('layout.master')
@section('judul')
Dashboard
@endsection
@section('content')
<section class="product_section layout_padding">
    <div class="container">
       <div class="row">
           
        @forelse($produk as $item)
          <div class="col-sm-6 col-md-4 col-lg-3">
             <div class="box">
                <div class="option_container">
                   <div class="options">
                      <a href="/produk/{{$item->id}}" class="option1">
                      Detail
                      </a>
                      <a href="/produk/{{$item->id}}" class="option2">
                      Buy Now
                      </a>
                   </div>
                </div>
                <div class="img-box">
                   <img src="{{asset('gambar/'. $item->foto)}}" alt="">
                </div>
                <div class="detail-box">
                   <h5>
                    {{$item->nama}}
                   </h5>
                   <h6>
                    Rp. {{$item->harga}}
                   </h6>
                </div>
             </div>
          </div>
          @empty
        <h4>Produk Belum ada</h4>
        @endforelse
       </div>
       <div class="btn-box">
          <a href="/produk">
          View All products
          </a>
       </div>
    </div>
 </section>


@endsection