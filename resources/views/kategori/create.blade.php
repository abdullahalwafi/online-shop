@extends('layout.master')
@section('judul')
TAMBAH KATEGORI
@endsection
@section('content')

<div class="card-body">
        
    <form action="/kategori" method="POST">
        @csrf
    <div class="form-group ">
        <label>Nama Kategori</label>
        <input type="text" name="nama" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" cols="20" rows="10" class="form-control"></textarea>
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</div>
@endsection