<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register/admin', function(){
    return view('auth.register_admin');
});

Route::group(['middleware' => ['checkadmin']], function () {
    // CRUD Kategori
    Route::get('/kategori/create', 'KategoriController@create'); //mengarah ke form tambah data
    Route::post('/kategori', 'KategoriController@store'); //menyimpan data form ke database table cast

    //read
    Route::get('/kategori', 'KategoriController@index'); // ambil data ke database kemudian tampil di blade


    //update
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit'); //mengarah form edit
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');

    //delete
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');
});
//CRUD Kategori show
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
// CRUD profile
Route::resource('/profile', 'ProfileController')->only(['index', 'update']);

//CRUD Produk
Route::get('/produk/create', 'ProdukController@create');
Route::post('/produk', 'ProdukController@store');

//read
Route::get('/produk', 'ProdukController@index'); // ambil data ke database kemudian tampil di blade
Route::get('/produk/{produk_id}', 'ProdukController@show');

//update
Route::get('/produk/{produk_id}/edit', 'ProdukController@edit'); //mengarah form edit
Route::put('/produk/{produk_id}', 'ProdukController@update');

//delete
Route::delete('/produk/{produk_id}', 'ProdukController@destroy');
Auth::routes();

// komentar
Route::get('/produk/{{produk_id}}', 'KomentarController@index');
Route::get('/testimonial', 'KomentarController@testimonial');
Route::resource('komentar', 'KomentarController')->only(['store']);

// order like
Route::resource('order', 'OrderController');
Route::resource('like', 'LikeController')->only(['index','store']);
