<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "Profile";
    protected $fillable = ["umur", "alamat", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
