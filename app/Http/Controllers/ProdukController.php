<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Like;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('checkadmin')->except(['index', 'show']);
    }

    public function index()
    {
        $produk = Produk::all();

        return view('produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('produk.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $fotoName = time() . '.' . $request->foto->extension();
        $request->foto->move(public_path('gambar'), $fotoName);

        $produk = new Produk;

        $produk->nama = $request->nama; //$request dari blade, $cast dari database
        $produk->harga = $request->harga;
        $produk->deskripsi = $request->deskripsi;
        $produk->kategori_id = $request->kategori_id;
        $produk->foto = $fotoName;


        $produk->save();

        return redirect('/produk')->with('success', 'Sukses tambah produk!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::findOrFail($id);
        $total_like = Like::where('produk_id', $id)->count();
        return view('produk.show', compact('produk', 'total_like'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::all();

        $produk = Produk::findOrFail($id);

        return view('produk.edit', compact('produk', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required',
            'foto' => '|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $produk = Produk::find($id);
        if ($request->has('foto')) {

            $fotoName = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('gambar'), $fotoName);


            $produk->nama = $request->nama; //$request dari blade, $cast dari database
            $produk->harga = $request->harga;
            $produk->deskripsi = $request->deskripsi;
            $produk->kategori_id = $request->kategori_id;
            $produk->foto = $fotoName;
        } else {
            $produk->nama = $request->nama; //$request dari blade, $cast dari database
            $produk->harga = $request->harga;
            $produk->deskripsi = $request->deskripsi;
            $produk->kategori_id = $request->kategori_id;
        }



        $produk->update();

        return redirect('/produk')->with('success', 'Sukses edit produk!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
